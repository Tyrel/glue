#How to use Glue

```
>>> from glue import glue
>>> print glue("any","number","of","things","together")
any number of things together

>>> print glue("any","number","of","things","together",where="anywhere")
any number of things together anywhere

>>> print glue("any","number","of","things","together",["even",["nested","lists"]])
any number of things together even nested lists

>>> print glue("any","number","of","things","together", ["even",["nested","lists"]], "and numbers", 12345)
any number of things together even nested lists and numbers 12345
```

# Coverage
```
Name    Stmts   Miss  Cover   Missing
-------------------------------------
glue       13      0   100%   
tests      24      0   100%   
-------------------------------------
TOTAL      37      0   100%   
```

#Protip
You should probably just use [str.join](https://docs.python.org/2/library/stdtypes.html#str.join) as this is satire.

Tyrel Souza 2015
